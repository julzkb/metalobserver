# Metal Observer

This project scrapes various record labels for new LP releases using [Scrapy](https://scrapy.org/), stores the releases in a sqlite DB and posts to Telegram et Twitter on regular intervals. Easier than trying to keep track of all the record labels :)

Here are the labels currently supported :

- Century Media
- Osmose Production
- FDA records
- Nuclear Blast 
- Profound Lore
- Relapse Records
- Season of Mist
- Southernlord records

---

## Scraping

We'll be doing our scraping using Scrapy, which means we are going to run [spiders](https://docs.scrapy.org/en/latest/topics/spiders.html) .
You could run them manually ... but nobody does that, so let's put it in a crontab instead, the syntax for launching spiders is as such :

```
scrapy crawl osmose -a limit=2 --set DOWNLOAD_DELAY=3 -o osmose.jl
```

- We call, *scrapy crawl* . 
- The spider name *osmose* .
- Set a page limit, we don't want to be scraping all the pages all the time since we've already done that *-a limit=2*
- Play nicely with the server to not get banned *--set DOWNLOAD_DELAY=3* 
- Output to a jsonlines file *-o osmose.jl*

---

## Posting 

**post.py** will be looking for these *ENV VARIABLES* 

Telegram:
- BOT_TOKEN
- BOT_CHATID

Twitter ( via [Tweepy](https://www.tweepy.org/))

(*pay attention to the coma separated values*)
- TWITTER_OAUTH
- TWITTER_ACCESS_TOKEN

Put them in a file called *app-env.sh* like so :

```
export BOT_TOKEN="my token"
export BOT_CHATID="@mybotchatid"

export TWITTER_OAUTH="oauthtoken,consumescret"
export TWITTER_ACCESS_TOKEN="accesstoken1,accesstoken2"
```



