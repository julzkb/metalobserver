import unittest
import ingest
import post


class TestIngest(unittest.TestCase):
    def test_get_label1(self):
        x = ingest.get_label('strange label')
        self.assertIs(x, 'strange label')

    def test_get_label2(self):
        x = ingest.get_label('osmose')
        self.assertTrue(x, 'osmose production')


class TestPost(unittest.TestCase):
    def test_get_one_album(self):
        x = post.get_one_album()
        self.assertEqual(len(x.keys()), 7)


if __name__ == "__main__":
    unittest.main()
