import scrapy
import re
from datetime import datetime


class ProfoundSpider(scrapy.Spider):
    name = "profound"


    def start_requests(self):
        baseurl = 'https://www.profoundlorerecords.com/products-page/vinyl/'
        yield scrapy.Request(url=baseurl, callback=self.parse)


    def parse(self, response):

        artistalbums = response.xpath('//*[@id="default_products_page_container"]/div[3]/div[*]/h2/a/text()').getall()
        images = response.xpath('//*[@class="imagecol"]/a/@href').getall()
        links = response.xpath('//*[@id="default_products_page_container"]/div[3]/div[*]/h2/a/@href').getall()
        prices = response.xpath('//*[@class="wpsc_product_price"]/p/span/text()').getall()

        for artistalbum, price, image, link in zip(artistalbums, prices, images, links):
            if not link:
                continue
            if "TAPE" in artistalbum:
                pass
            if "tape" in link:
                pass
            else:
                artist, album = "", ""
                p = 0.0
                print(artistalbum)
                if " – " in artistalbum:
                    thesplit = artistalbum.split("–")
                    artist = thesplit[0].strip().title()
                    m = re.search("\d", thesplit[1])
                    if m:
                        album = thesplit[1][0:m.start()].strip()
                    else:
                        if "LP" in thesplit[1]:
                            album = thesplit[1].split('LP')[0].strip()
                        else:
                            album = thesplit[1].strip()

                if "$" in price:
                    p = float(price.split('$')[1])

                yield {'image': image, 'album': album, 'artist': artist, 'price': p, 'currency': "usd", 'link': link, 'scrape_time':datetime.now()}
