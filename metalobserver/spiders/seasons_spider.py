import scrapy
from datetime import datetime

class SeasonsSpider(scrapy.Spider):
    name = "seasons"

    def start_requests(self):
        baseurl = 'https://shop.season-of-mist.com/music?cat=5&limit=30&p='
        fluff = '&style=6%2C7%2C9%2C11%2C12%2C13%2C14%2C14089%2C14090%2C14092%2C14093%2C14094'
        urls = []
        for x in range(1,int(self.limit)):
            urls.append(baseurl+str(x)+fluff)
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)


    def parse(self, response):
        tout = response.xpath('//*/h2[@class="product-name"]/a/@title').getall()
        links = response.xpath('//*/h2[@class="product-name"]/a/@href').getall()
        images = response.xpath('//*/div[@class="product-image-container"]/img/@src').getall()
        prices = response.xpath('//*/span[@class="price"]/text()').getall()

        #float(response.xpath('//*/span[@class="price"]/text()').getall()[0].split('€')[1])

        for i, im, p, l  in zip(tout, images, prices, links):
            s = i.split("-")
            artist= s[0].title()
            album = s[1].title()
            form = s[2].title()
            price = float(p.split('€')[1])

            yield {'album': album, 'artist': artist, 'format': form, 'image' : im, 'price': price, 'link': l, 'currency': "eur", 'scrape_time':datetime.now()}

