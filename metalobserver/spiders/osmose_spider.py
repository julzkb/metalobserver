import scrapy
from datetime import datetime


class OsmoseSpider(scrapy.Spider):
    name = "osmose"

    def start_requests(self):
        baseurl = 'https://www.osmoseproductions.com/liste/?lng=2&categ_rech=0&alpha=0&fmt=990001&srt=2&page='
        # baseurl = 'https://www.osmoseproductions.com/liste/?lng=2&categ_rech=0&alpha=0&fmt=990001&srt=2&page=1'
        urls = []
        for x in range(1, int(self.limit)+1):
            urls.append(baseurl+str(x))
        for url in urls:
            print(url)
            yield scrapy.Request(url=url, callback=self.parse)

        # yield scrapy.Request(url=baseurl, callback=self.parse)

    def parse(self, response):
        artists = response.xpath(
            '//*[@id="paginCorpA1"]/div[*]/div/div[2]/div[1]/div[2]/a/span[1]/text()').getall()
        albums = response.xpath(
            '//*[@id="paginCorpA1"]/div[*]/div/div[2]/div[1]/div[2]/a/span[2]/text()').getall()
        links = response.xpath(
            '//*[@id="paginCorpA1"]/div[*]/div/div[2]/div[1]/div[2]/a/@href').getall()

        for artist, album, link in zip(artists, albums, links):
            form = ""
            if not link:
                continue
            else:
                if "." in album:
                    form = album.split(".")[1].strip()
                    album = album.split(".")[0].strip()
                else:
                    form = ""
                yield response.follow(link, self.get_image, meta={'artist': artist.title(), 'album': album, 'format': form, 'link': link})

    def get_image(self, response):
        artist = response.meta['artist']
        album = response.meta['album']
        form = response.meta['format']
        link = response.meta['link']
        price = int(response.xpath(
            '//*/span[@class="cufonCd "]/text()').getall()[0].strip().split('.')[0])
        for im in response.xpath('//*[@id="img_product_page_osmose"]/img[@id="id_photo_principale"]/@src').getall():
            yield {'image': im, 'album': album, 'artist': artist, 'format': form, 'price': price, 'currency': "eur", 'link': link, 'scrape_time': datetime.now()}
