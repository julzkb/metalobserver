import scrapy
from datetime import datetime

class RelapseSpider(scrapy.Spider):
    name = "relapse"

    def start_requests(self):
        baseurl = 'https://store.relapse.com/c/261,250,1008,263/lp?pg='
        urls = []
        for x in range(1,int(self.limit)+1):
            urls.append(baseurl+str(x)+'&lim=72&sort=newest')
        for url in urls:
            print(url)
            yield scrapy.Request(url=url, callback=self.parse)


    def parse(self, response):
        artists = response.xpath('/html/body/div[5]/div/div/div[1]/div[3]/div[*]/a/div[2]/div[1]/text()').getall()
        albums = response.xpath('/html/body/div[5]/div/div/div[1]/div[3]/div[*]/a/div[2]/div[2]/text()').getall()

        images = response.xpath('/html/body/div[5]/div/div/div[1]/div[3]/div[*]/a/div[1]/div/img/@src').getall()
        prices = response.xpath('/html/body/div[5]/div/div/div[1]/div[3]/div[*]/a/div[2]/div[4]/text()').getall()
        links = response.xpath('/html/body/div[5]/div/div/div[1]/div[3]/div[*]/a/@href').getall()

        for artist, album, image, p, l in zip(artists, albums, images, prices, links):
            price = float(p.strip().split('$')[1])
            link = "https://store.relapse.com" + l
            yield {'image': image, 'album': album, 'artist': artist, 'price': price, 'currency': "usd", "link": link, 'scrape_time':datetime.now()}


