import scrapy
from datetime import datetime


class FdaSpider(scrapy.Spider):
    name = "fda"

    def start_requests(self):
        baseurl = 'https://fda-records.com/en/12inch-deathmetal-blackmetal/?view_mode=tiled&listing_sort=date_desc' \
                  '&filter_id=0&listing_count=96&page= '
        urls = []
        for x in range(1, int(self.limit) + 1):
            urls.append(baseurl + str(x))
        for url in urls:
            print(url)
            yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):

        artistsalbums = response.xpath(
            '//*[@id="main"]/div/div[4]/div/div[*]/form/div/div/div/div[1]/div[1]/a/@title').getall()
        links = response.xpath('//*[@id="main"]/div/div[4]/div/div[*]/form/div/div/div/div[1]/div[1]/a/@href').getall()
        prices = list(filter(lambda s: len(s) > 1, response.xpath(
            '//*[@id="main"]/div/div[4]/div/div[*]/form/div/div/div/div[2]/div[1]/span/text()').getall()))

        for artistalbum, link, price in zip(artistsalbums, links, prices):
            artist, album = "", ""
            p = price.strip()
            if "only" in p:
                prix = float(p.replace("only", "").strip().split(" ")[0].replace(',', '.'))
            else:
                prix = float(p.split(" ")[0].replace(',', '.'))
            if not link:
                continue
            else:
                if "-" in artistalbum:
                    thesplit = artistalbum.split('-')
                    artist = thesplit[0].title()
                    album = thesplit[1]
                else:
                    pass
                yield response.follow(link, self.get_image,
                                      meta={'artist': artist, 'album': album, 'link': link, 'price': prix})

    def get_image(self, response):
        artist = response.meta['artist'].strip()
        album = response.meta['album'].strip()
        link = response.meta['link']
        price = response.meta['price']
        imageurl = response.xpath('//*[@id="product_image_swiper"]/div/div/div/a/@href').get()
        im = "https://fda-records.com/" + imageurl
        yield {'image': im, 'album': album, 'artist': artist, 'price': price, 'currency': "eur", 'link': link,
               'scrape_time': datetime.now()}
