import scrapy
from datetime import datetime


class SouthernSpider(scrapy.Spider):
    name = "southern"

    def start_requests(self):
        baseurl = 'https://southernlord.com/releases/page/'
        urls = []
        for x in range(1,int(self.limit)+1):
            urls.append(baseurl+str(x))
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)


    def parse(self, response):
        artists = response.xpath('//*[@id="main"]/ul/li[*]/a/h3/text()').getall()
        albums = response.xpath('//*[@id="main"]/ul/li[*]/a/h4/text()').getall()
        links = response.xpath('//*[@id="main"]/ul/li[*]/a/@href').getall()

        for artist, album, link in zip(artists, albums, links):
            if not link:
                continue
            yield response.follow(link, self.get_image, meta={'artist': artist.title(), 'album': album, 'link': link })


    def get_image(self, response):
        artist = response.meta['artist']
        album = response.meta['album']
        link = response.meta['link']
        im = response.xpath('//div[1]/a/@href').getall()[1]
        price = float(response.xpath('//*/div[2]/p/span/text()').getall()[-1])

        yield {'image': im, 'album': album, 'artist': artist, 'link': link, 'price': price, 'currency': "usd", 'scrape_time':datetime.now()}

