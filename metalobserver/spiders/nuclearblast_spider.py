import scrapy
from datetime import datetime

class NuclearSpider(scrapy.Spider):
    name = "nuclear"

    def start_requests(self):
        baseurl = 'https://www.nuclearblast.de/en/shop/artikel/gruppen/80045.'
        urls = []
        # for x in range(1,54):
        for x in range(1,int(self.limit)+1):
            urls.append(baseurl+str(x)+".vinyl.html")
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)


    def parse(self, response):
        artists = response.xpath('//*/h2/a/text()').getall()
        albums = response.xpath('//*/h2/span/text()').getall()
        links = response.xpath('//*/h2/a/@href').getall()

        for artist, album, link in zip(artists, albums, links):
            formlist, albumlist = [], []
            if not link:
                continue
            for x in album.split(" "):
                if x.isupper():
                    formlist.append(x.title())
                else:
                    albumlist.append(x)

            yield response.follow(link, self.get_image, meta={'artist': artist.title(), 'album': " ".join(albumlist).title(), 'form': " ".join(formlist), 'link': link })



    def get_image(self, response):
        artist = response.meta['artist']
        album = response.meta['album']
        form = response.meta['form']
        link = response.meta['link']
        price = float(response.xpath('//*[@id="content-frame"]/div[1]/form/div[1]/div[2]/div[1]/span[1]/text()').getall()[0].replace(u'\xa0', ' ').split(' ')[0])

        for im in response.xpath('//*/div[@class="article-media"]//*[contains(@class,"jqzoom")]/@href').getall():
            yield {'image': im, 'album': album, 'artist': artist, 'format': form, 'price': price, 'currency': "eur", 'link': link, 'scrape_time':datetime.now()}
