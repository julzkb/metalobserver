import scrapy


class CenturySpider(scrapy.Spider):
    name = "century"

    def start_requests(self):
        baseurl = 'https://www.centurymedia.com/releases.aspx'

        yield scrapy.Request(url=baseurl, callback=self.parse)

    def parse(self, response):
        artists = response.xpath('//*[@class="ArtistName"]/a/text()').getall()
        albums = response.xpath('//*[@class="Title"]/a/text()').getall()
        links = response.xpath('//*[@class="page"]/@href').getall()
        print(artists)
        for artist, album, link in zip(artists, albums, links):
            if not link:
                continue
            yield response.follow(link, self.get_image, meta={'artist': artist, 'album': album})

    def parse_results(self, response):
        print("parse results")
        print(response.css('input#__VIEWSTATE::attr(value)').extract_first())

        artists = response.xpath('//*[@class="ArtistName"]/a/text()').getall()
        albums = response.xpath('//*[@class="Title"]/a/text()').getall()
        links = response.xpath('//*[@class="page"]/@href').getall()
        print(artists)
        print(albums)
        print(links)
