import tweepy
import os
import logging
import requests


def twitter_login():

    # Authenticate to Twitter
    twitter_oauth = os.environ['TWITTER_OAUTH'].split(',')
    twitter_access_token = os.environ['TWITTER_ACCESS_TOKEN'].split(',')

    auth = tweepy.OAuthHandler(twitter_oauth[0], twitter_oauth[1])
    auth.set_access_token(twitter_access_token[0], twitter_access_token[1])

    api = tweepy.API(auth)
    api.verify_credentials()
    logging.info(" Twitter Authentication OK ")
    return api


def tweet_image(artist, album, image, price, cur, label, link):
    '''
    Tweets album
    Input : artist, album, image, price, cur, label, link
    Returns : True if posted
    '''

    filename = 'heavyvinyl.jpg'
    request = requests.get(image, stream=True)
    if request.status_code == 200:
        with open(filename, 'wb') as im:
            for chunk in request:
                im.write(chunk)
        api = twitter_login()

        message = "{}\n{}\n{} : {} {}\n\n{}".format(
            artist, album, '#' + label.replace(' ', ''), str(price), cur, link)

        api.update_with_media(filename, status=message)
        os.remove(filename)

        return True
    else:
        logging.error("Unable to download image")
        return None
