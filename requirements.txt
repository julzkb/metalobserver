jsonlines==1.2.0
Scrapy==1.7.3
requests==2.22.0
requests-oauthlib==1.2.0
requests-unixsocket==0.1.5
tweepy==3.8.0
