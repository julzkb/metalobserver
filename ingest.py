import json
import sqlite3
import sys
import itertools
import operator
import logging
import jsonlines

db = "metalobserver.db"


logging.basicConfig(level=logging.INFO)


def get_label(l):
    '''
    Returns the long name of the label
    Input : label string as scraped
    Returns : long label name
    '''

    labels = {'osmose': 'osmose production',
              'nuclear': 'nuclear blast',
              'relapse': 'relapse records',
              'season': 'season of mist',
              'southern': 'southern lord records',
              'profound': 'profound lore records'}

    for i in labels.keys():
        if i in l:
            return labels[i]

        else:
            return l


def dedup(jlfile):
    '''
    Deduplicates input file
    Input : jsonline file that we scrapped
    Returns : None ( writes the file instead)
    '''
    with open(jlfile) as content_file:
        alllines = []
        for line in content_file:
            jo = json.loads(line)
            alllines.append(jo)
        l = alllines

        logging.info("Dedup starting :")
        logging.info("Original length : {}".format(len(l)))

        getvals = operator.itemgetter('artist', 'album')
        l.sort(key=getvals)
        result = []
        for k, g in itertools.groupby(l, getvals):
            result.append(g.__next__())
        l[:] = result

        logging.info("New length : {}".format(len(l)))

    with jsonlines.open(jlfile, mode='w') as writer:
        writer.write_all(l)


def main():

    selected_file = sys.argv[1]
    dedup(selected_file)
    logging.info("Started Ingest")

    with open(selected_file) as content_file:
        records = []

        conn = sqlite3.connect(db)
        c = conn.cursor()

        for line in content_file:
            jo = json.loads(line)

            c.execute("SELECT rowid FROM releases WHERE image  = ?",
                      (jo.get('image'),))
            data = c.fetchall()
            if len(data) == 0:
                logging.info('This image hasnt been found, adding row to db')
                record = (jo.get('artist'), jo.get('album'), jo.get('image'),
                          float(jo.get('price')), jo.get(
                    'currency'), get_label(selected_file),
                    jo.get('link'), jo.get('scrape_time'), 0)

                records.append(record)
            else:
                logging.info("Record already in the DB, passing ...")

        try:
            conn = sqlite3.connect(db)
            c = conn.cursor()
            c.executemany(
                "INSERT INTO releases VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)", records)
            conn.commit()
            conn.close()
        except:
            raise

    logging.info("Finished Processing")


if __name__ == '__main__':
    main()
