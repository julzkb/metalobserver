import sqlite3
import logging
import requests
import os
import post_to_twitter

db = "metalobserver.db"
logging.basicConfig(level='INFO')


def get_one_album():
    '''
    Connects to the db and return a random album
    Input : None
    Returns : dict
    '''
    conn = sqlite3.connect(db)
    c = conn.cursor()
    c.execute("SELECT * FROM releases WHERE published=0 ORDER BY RANDOM() LIMIT 1;")
    data = c.fetchone()
    conn.close()

    album = {'artist': data[0].rstrip(),
             'album': data[1].strip(),
             'image': data[2].rstrip(),
             'price': str(data[3]),
             'cur': data[4].rstrip(),
             'label': data[5].title(),
             'link': data[6].rstrip()
             }

    return album


def telegram_bot_sendalbum(artist, album, image, price, cur,  label, link):
    """
    Publishes new record to Telegram
    Returns : Telegram's response
    """
    bot_token = os.environ['BOT_TOKEN']
    bot_chatID = os.environ['BOT_CHATID']
    bot_message = f'*{artist}*\n{album}\n\n_{label}_ : {price} {cur} \n\n {link}'
    send_photo = f'https://api.telegram.org/bot{bot_token}/sendPhoto?chat_id={bot_chatID}&photo={image}&caption={bot_message}&parse_mode=Markdown'
    response = requests.get(send_photo)

    return response.json()


def mark_as_published(image):
    '''
    Marks inputs record as published, uses image to identify the record
    Input : Image to look for
    Returns : None
    '''
    conn = sqlite3.connect(db)
    c = conn.cursor()
    logging.info("Marking record : {} as published in the db.".format(image))
    c.execute('UPDATE releases SET published=1 WHERE image=?', (image,))
    conn.commit()
    conn.close()


def main():

    data = get_one_album()

    # send to telegram
    send_telegram = telegram_bot_sendalbum(data['artist'], data['album'], data['image'],
                                           data['price'], data['cur'], data['label'], data['link'])
    logging.info(send_telegram)

    # sent to Twitter
    send_twitter = post_to_twitter.tweet_image(
        data['artist'], data['album'], data['image'], data['price'], data['cur'], data['label'], data['link'])

    if send_twitter is True:
        mark_as_published(data['image'])


if __name__ == "__main__":
    main()
